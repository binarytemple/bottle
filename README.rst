===========
Bottle Work 
===========

My experiments with Bottle and Mongodb, also used to simulate the bitbucket
rest API locally.

Install the requisites::
  apt-get install mongodb build-essential python-dev && pip install --upgrade pymongo bottle

Querying the mongodb instance::

  dsktp# mongo mydatabase
  MongoDB shell version: 2.0.4
  connecting to: mydatabase
  > db.getCollectionNames()
  [ "documents", "system.indexes" ]
  > db.documents()
  Wed May  2 19:15:04 TypeError: db.documents is not a function (shell):1
  > db.documents.find()
  { "_id" : "doc1", "name" : "Test Document 1" }
  { "_id" : "doc2", "name" : "Test Document 1" }


Talking to putserver
--------------------

Test putserver with the following command:

     curl -X PUT -H 'Content-Type: application/json' --data-binary  '{"_id":"1","name":"Test"} ' http://localhost:9999/documents