#!/usr/bin/python

import json
import bottle
from bottle import route, run, request, abort
from pymongo import Connection


from bottle import static_file
@route('/<filename:path>')
def send_html(filename):
    return static_file(filename, root='./static', mimetype='text/html')

run(host='localhost', port=8080)
