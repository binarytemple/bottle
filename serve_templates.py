#!/usr/bin/python

import json
import bottle
from bottle import route, run, request, abort
from pymongo import Connection
from bottle import static_file,template

    

("Origin") 
 "Access-Control-Allow-Origin: Any" 
 "Access-Control-Allow-Credentials" -> "true"
("Access-Control-Request-Headers") map { value =>
 "Access-Control-Allow-Headers" -> value
("Access-Control-Request-Method") map { value =>
 "Access-Control-Allow-Methods" -> (value + ", GET, OPTIONS")
 "Access-Control-Allow-Origin" )



@route('/hello')
@route('/hello/<name>')
def hello(name='World'):
    return template('hello', name=name)

@route('/<filename:path>')
def send_html(filename):
    return static_file(filename, root='./static', mimetype='text/html')

run(host='localhost', port=8080)
