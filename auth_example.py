#!/bin/python

from bottle import route, run, response
import bottle
import json

je = json.JSONEncoder()
jd = json.JSONDecoder()

# This example demonstrates POSTs and Authentication

# Expects
# curl -d '{"auth_secret": "foo", "auth_id": "foo@bar.com"}' https://localhost:8080/foo/verify/
@route('/foo/verify/', method='POST')
def verify():
    data = jd.decode(bottle.request.body.read())
    if data['auth_secret'] != 'foo' or data['auth_id'] != 'foo@bar.com':
        bottle.response.status = 403
        ret = {'status': 'access denied'}
        return je.encode(ret)

    ret = {'session': 'sessionx', 'user_id': 'userx'}
    return je.encode(ret)

@route('/foo/bar/')
def search():
    auth = bottle.request.auth
    if auth == ("userx", "sessionx"):
        ret = {"result": ["one", "two", "three"]}
        return je.encode(ret)
    else:
        bottle.response.status = 403
        ret = {'status': 'access denied'}
        return je.encode(ret)


run(host='localhost', port=8080, debug=True, reloader=True)
